// @ts-nocheck

// title      : pvcParts
// author     : John Cole
// license    : ISC License
// file       : pvcParts.jscad

/* exported main, getParameterDefinitions */

function getParameterDefinitions() {
  return [
    {
      name: 'part',
      type: 'choice',
      values: [
        'Elbow',
        'Threeway',
        'Cap',
        'ThreewayPair',
        'ClipCap',
        'Clip',
        'LongClip',
        'DoubleClip',
        'DrillGuide',
        'Gusset'
      ],
      initial: 0,
      caption: 'Part:'
    },
    {
      name: 'resolution',
      type: 'choice',
      values: [16, 24, 32, 64, 128],
      initial: 0,
      caption: 'Resolution:'
    }
  ];
}

var Plugs = 3; // [2:5]
var Outer_Diameter = 21;
var Inner_Diameter = 15.25;
var Angle1 = 180; // [0,45,72,90,135,180]
var Angle2 = 90; // [0,45,72,90,135,180]
var InnerRadius = Inner_Diameter / 2;
var OuterRadius = Outer_Diameter / 2;
var taper = InnerRadius - 2;
var CutBox = InnerRadius + 5;
var InvCutBox = CutBox - (CutBox + CutBox) + 0.5;

function main(params) {
  var resolutions = [[6, 16], [8, 24], [12, 32], [24, 64], [48, 128]];
  var resolution = 128; //Number.parseInt(params.resolution);
  // console.log("params", params, { resolution });

  CSG.defaultResolution3D = params.resolution;
  CSG.defaultResolution2D = params.resolution;
  util.init(CSG);
  // console.log("CSG.defaultResolution3D", CSG.defaultResolution3D);
  // console.log("CSG.defaultResolution2D", CSG.defaultResolution2D);

  // return pipe();
  // return Clip(Outer_Diameter);
  // return ClipCap(Outer_Diameter, Inner_Diameter, taper);
  // return testFit(Inner_Diameter);
  // return drillGuide(Outer_Diameter);
  // return DoubleClip(Outer_Diameter);

  var parts = {
    ClipCap: () => ClipCap(Outer_Diameter, Inner_Diameter, taper),
    Clip: () => Clip(Outer_Diameter).parts.clip,
    LongClip: () => {
      var clip = Clip(Outer_Diameter,  50)
      var offset = Outer_Diameter / 2;
      var p = pipe('pipe').bisect('z', 10).parts.negative.rotateX(90).rotateZ(-45).align(clip.parts.clip, 'xyz').translate([2.25, 2.25, 0]);
      var t = util.nearest.over(1.5);
      var p2 = p.enlarge([t,t,t])
      return clip.parts.clip.union(p2).subtract([clip.parts.pipe, clip.parts.cube]).subtract(p);
    },
    DoubleClip: () => DoubleClip(Outer_Diameter),
    DrillGuide: () => drillGuide(Outer_Diameter),
    ThreewayPair: () => pipe('threewayPair'),
    Threeway: () => pipe('threeway'),
    Elbow: () => pipe('elbow'),
    Cap: () => pipe('cap'),
    Gusset: () => Gusset(Outer_Diameter, Inner_Diameter, taper)
  };

  return parts[params.part]();
}

function Gusset(OD, ID, taper) {
  var clip = Clip(OD, 30).rotate(util.unitCube(), 'z', 45);
  var pipe = Parts.Cylinder(OD, 25);
  var plug = Plug(Outer_Diameter, Inner_Diameter, taper)
    .subtract(number8tap())
    .stretch('z', 15, 5)
    .rotateY(-45)
    .translate([-1, 0, 6]);
  // .snap(clip.parts.clip, 'x', 'outside+')
  // .align(clip.parts.clip, 'z')
  // .translate([7, 0, 0]);
  var g = clip.parts.clip.union([plug]).subtract(
    clip
      .combine('pipe,cube')
      .stretch('z', 10)
      .translate([0, 0, -5])
  );

  return union(g, g.mirroredX().translate([25, 0, 0]));
}

// function Gusset(OD) {
//   var clip1 = Clip(OD)
//     .rotate('clip', 'z', -45)
//     .rotate('clip', 'y', -90)
//     .translate([50, 0, 0]);

//   var h = clip1.parts.clip.getBounds();
//   clip1.translate([0, 0, -h[0].z]); // zero()
//   // var clip2 = clip1.parts.clip.mirroredY().rotateZ(90);
//   var bounds1 = clip1.parts.clip.getBounds();
//   var clip2 = clip1
//     .clone()
//     .rotate('clip', 'z', 90)
//     .translate([-bounds1[0].x + bounds1[0].y, bounds1[0].x - bounds1[0].y, 0]);
//   var bounds2 = clip2.parts.clip.getBounds();

//   console.log(clip1.parts.clip.getBounds(), clip1.parts.clip.size());
//   // var wallThickness = util.nearest.over(1.5);
//   var gussetThickness = clip1.parts.clip.size().z / 2;

//   let p1 = polygon([
//     [bounds1[1].x, bounds1[1].y],
//     [bounds1[0].x, bounds1[1].y],
//     [bounds2[1].x, bounds2[0].y],
//     [bounds2[1].x, bounds2[1].y]
//   ]);

//   var p2 = polygon([
//     [bounds1[1].x, bounds1[1].y],
//     [bounds1[0].x, bounds1[1].y],
//     [bounds1[0].x, bounds1[0].y],
//     [bounds1[1].x, bounds1[0].y]
//   ]);

//   var p3 = polygon([
//     [bounds2[1].x, bounds2[1].y],
//     [bounds2[0].x, bounds2[1].y],
//     [bounds2[0].x, bounds2[0].y],
//     [bounds2[1].x, bounds2[0].y]
//   ]);

//   var c1 = util
//     .poly2solid(p2, p2, gussetThickness)
//     .color('blue')
//     .bisect('y').parts.negative;

//   var c2 = util.poly2solid(p3, p3, gussetThickness).color('green');

//   var g = util.poly2solid(p1, p1, gussetThickness);
//   var g1 = g.rotateZ(45).bisect('x');
//   var g_p = g1.parts.positive.bisect('x', 10);
//   var g_n = g1.parts.negative.bisect('x', -10);
//   var lapjoint = union(g_n.parts.positive, g_p.parts.negative).bisect('z');
//   var gusset = util
//     .group({
//       positive: union(g_p.parts.positive, lapjoint.parts.negative).color(
//         'blue'
//       ),
//       negative: union(g_n.parts.negative, lapjoint.parts.positive).color('red')
//     })
//     .rotate(util.unitCube(), 'z', -45);

//   return union([
//     clip1
//       .combine('clip')
//       .union(c1)
//       .subtract(clip1.parts.pipe),
//     clip2.parts.clip
//       .union(c2.bisect('x').parts.positive)
//       .subtract(clip2.parts.pipe),

//     gusset.combine()
//   ]);
// }

function DoubleClip(OD) {
  var clip = Clip(OD).parts.clip.rotateZ(-45);
  var wallThickness = util.nearest.over(1.5);
  return union([
    clip,
    clip.mirroredX().snap(clip, 'x', 'outside+'),
    Parts.Cube([wallThickness, 8, 25])
      .align(clip, 'xyz')
      .snap(clip, 'x', 'outside+')
      .translate([wallThickness / 2, 0, 0])
  ]);
}

function ClipCap(OD, ID, taper) {
  var clip = Clip(OD);
  var pipe = Parts.Cylinder(OD, 25);
  var plug = Plug(Outer_Diameter, Inner_Diameter, taper)
    .subtract(number8tap())
    .rotateY(-90)
    .snap(clip.parts.clip, 'x', 'outside+')
    .align(clip.parts.clip, 'z')
    .translate([OD * 0.45, 0, 0]);
  return clip.parts.clip.union([plug]).subtract(pipe);
}

function Clip(OD, height = 25) {
  // console.log("Nozzel", util);
  var pipe = Parts.Cylinder(OD, height, {
    resolution: CSG.defaultResolution2D
  });
  var thickness = util.nearest.over(3.5, 0.35);
  console.log('Clip thickness', thickness);
  var body = Parts.Cylinder(OD + thickness, height, {
    resolution: CSG.defaultResolution2D
  }).subtract([pipe]);

  var cube = Parts.Cube([
    height - thickness,
    height - thickness,
    height
  ]).translate([thickness / 2, thickness / 2, 0]);
  return util.group('clip,pipe', {
    clip: union([body, Parts.Cube([height, height, height])])
      .subtract([cube])
      .subtract(pipe)
      .intersect(
        Parts.Cylinder(OD * 1.5, height, {
          resolution: CSG.defaultResolution2D
        })
      ),
    pipe,
    cube
  });
}

function pipe(part) {
  // return plug(Outer_Diameter, Inner_Diameter, taper);
  // return testFit(Inner_Diameter);
  // return number8tap();
  // return my_2_way_90_elbow(Outer_Diameter, Inner_Diameter, taper, InvCutBox);
  // return tappedPlug(Outer_Diameter, Inner_Diameter, taper);
  // return my_3_way_0_90_180_elbow(
  //   Outer_Diameter,
  //   Inner_Diameter,
  //   taper,
  //   InvCutBox
  // );
  // return drillGuide(Outer_Diameter);
  var dg = drillGuide(Outer_Diameter).Zero();
  var elbow = my_2_way_90_elbow(
    Outer_Diameter,
    Inner_Diameter,
    taper,
    InvCutBox
  )
    .rotateY(-90)
    .Center()
    .Zero();

  var elbowPair = elbow
    .union(
      elbow
        .rotateZ(180)
        .snap(elbow, 'xy', 'inside-')
        .translate([0, 20, 0])
    )
    .align(dg, 'xy');

  var threeway = my_3_way_0_90_180_elbow(
    Outer_Diameter,
    Inner_Diameter,
    taper,
    InvCutBox
  )
    .rotateY(-90)
    .Zero();
  var threewayPair = threeway
    .union(
      threeway
        .rotateZ(180)
        .snap(threeway, 'xy', 'inside-')
        .translate([Outer_Diameter, 20, 0])
    )
    .Center();

  var gap = 5;

  var parts = {
    dg: () => dg,
    threewayPair: () => threewayPair,
    elbow: () => elbow,
    threeway: () => threeway,
    cap: () => Cap(Outer_Diameter, Inner_Diameter, taper, InvCutBox),
    pipe :() => Parts.Cylinder(Outer_Diameter, 25, {
      resolution: CSG.defaultResolution2D
    }).color('white')
  };

  // return union([
  //   // dg
  //   // elbowPair.snap(dg, "x", "outside-", gap)
  //   // elbowPair
  //   //   .snap(dg, "x", "outside-", gap * 3)
  //   //   .snap(dg, "y", "outside+", -gap),
  //   // elbowPair.snap(dg, "y", "outside+", -gap),
  //   // elbowPair
  //   //   .snap(dg, "x", "outside+", -gap * 3)
  //   //   .snap(dg, "y", "outside+", -gap),
  //   threewayPair //.snap(dg, "x", "outside+", -gap).translate([0, 20, 0]),
  //   // threewayPair.snap(dg, "y", "outside-", gap)
  // ]);
  // return union([
  //   dg,
  //   elbow.snap(dg, "x", "outside-", gap),
  //   threeway.snap(dg, "x", "outside+", -gap)
  // ]);

  return parts[part]();
}

function tappedPlug(OR, IR, TPR) {
  var plug = Plug(OR, IR, TPR);
  return union([plug, number8tap()]);
}

function testFit(InnerDiameter) {
  return union(
    [1, 0.75, 0.5, 0.25, 0, -0.25, -0.5].reduce((body, offset, index) => {
      body.push(
        Parts.Cylinder(InnerDiameter + offset * 2, 5, {
          resolution: CSG.defaultResolution2D
        }).translate([0, 0, index * 5])
      );
      return body;
    }, [])
  );
}

function Plug(Outer_Diameter, Inner_Diameter, taper) {
  /**
 module plug(OR, IR, TPR) {
  union(){
    cylinder(r1=IR, r2=IR, h=33, center=false);
    translate([0, 0, 33]){
      cylinder(r1=IR, r2=TPR, h=5, center=false);
    }
    cylinder(r1=OR, r2=OR, h=10, center=false);
  }
}
 */

  var base = Parts.Cylinder(Inner_Diameter, 33, {
    resolution: CSG.defaultResolution2D
  });
  return base
    .union([
      Parts.Cone(Inner_Diameter, taper * 2, 5, {
        resolution: CSG.defaultResolution2D
      }).snap(base, 'z', 'outside-'),
      Parts.Cylinder(Outer_Diameter, 10, {
        resolution: CSG.defaultResolution2D
      })
    ])
    .color('orange');
}

function number8tap(r = 1.39) {
  /*
module number8tap(r=1.39) {
    union() {
        cylinder(r1=r, r2=r, h=37, center=true);
        rotate([0, 90, 0]) {
            translate([0, -5, 0]) {
                cylinder(r1=r, r2=r, h=37, center=true);
            }
        }
    }
}
*/
  var tap = Parts.Cylinder(r * 2, 37, {
    resolution: CSG.defaultResolution2D
  })
    .rotateX(90)
    .Center()
    .translate([0, 0, 30]);
  return union([
    tap,
    tap
      .rotateZ(90)
      .align(tap, 'xyz')
      .translate([0, 0, -5])
  ]).color('red');
}

function Cap(OR, IR, TPR, ICB) {
  var plug = Plug(OR, IR, TPR);
  return union([
    // plug.rotateX(180),
    plug.rotateX(270),
    CSG.sphere({
      center: [0, 0, 0],
      radius: OR / 2,
      resolution: CSG.defaultResolution3D
    }).color('blue')
  ]).subtract([
    number8tap().rotateX(180),
    number8tap().rotateX(270),
    Parts.Cube([10, 80, 80])
      .Center('xyz')
      .translate([ICB, 0, 0])
      .color('red')
  ]);
}

function my_2_way_90_elbow(OR, IR, TPR, ICB) {
  /*
module my_2_way_90_elbow(OR, IR, TPR, ICB) {
  difference() {
    union(){
      rotate([180, 0, 0]){
        plug(OR, IR, TPR);
      }
      rotate([270, 0, 0]){
        plug(OR, IR, TPR);
      }
      sphere(r=OR);
    }

    translate([ICB, 0, 0]){
      cube([10, 30, 30], center=true);
    }
    
    translate([0, 30, 0]) {
        number8tap();
    }
    
    translate([0, 0, -30]) {
        rotate([-90, 0, 0]) {
            number8tap();
        }
    }
      
  }
}
*/
  var plug = Plug(OR, IR, TPR);
  return union([
    plug.rotateX(180),
    plug.rotateX(270),
    CSG.sphere({
      center: [0, 0, 0],
      radius: OR / 2,
      resolution: CSG.defaultResolution3D
    }).color('blue')
  ]).subtract([
    number8tap().rotateX(180),
    number8tap().rotateX(270),
    Parts.Cube([10, 80, 80])
      .Center('xyz')
      .translate([ICB, 0, 0])
      .color('red')
  ]);
}

function my_3_way_0_90_180_elbow(OR, IR, TPR, CB) {
  /*
module my_3_way_0_90_180_elbow(OR, IR, TPR, CB) {
  difference() {
    union(){
      rotate([0, 0, 0]){
        plug(OR, IR, TPR);
      }
      rotate([270, 0, 0]){
        plug(OR, IR, TPR);
      }
      rotate([180, 0, 0]){
        plug(OR, IR, TPR);
      }
      sphere(r=OR);
      
    }

    translate([CB, 0, 0]){
      cube([10, 30, 30], center=true);
    }
    translate([0,30,0]){
        number8tap();
      }
      
      translate([0, 0, -30]) {
        rotate([-90, 0, 0]) {
            number8tap();
        }
      }
      translate([0, 0, +30]) {
        rotate([90, 0, 0]) {
            number8tap();
        }
      }
  }
}
*/
  var plug = Plug(OR, IR, TPR);
  return union([
    plug,
    plug.rotateX(270),
    plug.rotateX(180),
    CSG.sphere({
      center: [0, 0, 0],
      radius: OR / 2,
      resolution: CSG.defaultResolution3D
    }).color('blue')
  ]).subtract([
    number8tap(),
    number8tap().rotateX(180),
    number8tap().rotateX(270),
    Parts.Cube([10, 80, 80])
      .Center('xyz')
      .translate([CB, 0, 0])
      .color('red')
  ]);
}

function drillGuide(OR) {
  /*
if (P == 0) {
        //union() {
            translate([0, 0, 10]) {
                difference() {
                    cylinder(r1=OR + 5.5, r2=OR + 5.5, h=28, center=false);
                    translate([0,0,-1]) {
                        cylinder(r1=OR + 0.5, r2=OR + 0.5, h=35, center=false);
                    }
                    translate([0, 0, 20]) {
                        rotate([90, 0, 0]) {
                            number8tap(2);
                        }
                    }
                }
            }
          //  my_3_way_0_90_180_elbow(OR, IR, TPR, ICB);
        //}
    }
*/
  var body = Parts.Cylinder(OR + 11, 28, {
    resolution: CSG.defaultResolution2D
  }).translate([0, 0, 10]);

  var pipe = Parts.Cylinder(OR + 1, 35, {
    resolution: CSG.defaultResolution2D
  }).align(body, 'xyz');
  return (
    body
      // .bisect('y')
      // .parts.negative
      .union([
        Parts.Cube([OR + 11, OR / 2, 28])
          .align(body, 'xyz')
          .snap(pipe, 'y', 'inside+')
          .snap(body, 'z', 'inside-'),
        Parts.Cube([OR, OR + 11, 10])
          .chamfer(2, 'z+')
          .chamfer(2, 'z-')
          .align(body, 'xyz')
          .snap(body, 'y', 'outside+', 10)
          .snap(body, 'z', 'inside-')
        // .translate([0, OR + 11, 0])
      ])
      .color('green')
      .subtract([
        Parts.Cylinder(OR + 1, 35, {
          resolution: CSG.defaultResolution2D
        })
          .align(body, 'xyz')
          .stretch('y', 11 * 2),
        Parts.Cube([28, 28, 28])
          .align(pipe, 'xyz')
          .snap(pipe, 'y', 'outside-'),
        util.rotateAround(number8tap(2), body, 'z', 90)
      ])

    // .union(util.rotateAround(number8tap(2), body, 'z', 90))
  );
}

// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// endinject
